# 『日本語倶楽部』
<table width="763" border="0" cellspacing="0" cellpadding="0" borderspacing="0" style="margin:0px;">
<tr align="center" valign="top">
<td><img src="images/hyoshi.png"></td>
<!--
<td colspan="9" valign="top"><img src="images/studyjp_hyoshi.jpg"></td>
<td><img src="images/United-States-of-America.png"></td>
<td><img src="images/United-Kingdom.png"></td>
<td><img src="images/Germany.png"></td>
<td><img src="images/France.png"></td>
<td><img src="images/Indonesia.png"></td>
<td><img src="images/Nepal.png" height="181" width="154"></td>
<td><img src="images/Vietnam.png"></td>
<td><img src="images/Bangladesh.png"></td>
<td><img src="images/India.png"></td>
</tr>
<tr align="center" valign="middle"> 
<td><img src="images/Sri_Lanka.png"></td>
<td><img src="images/China.png"></td>
<td><img src="images/South-Korea.png"></td>
<td><img src="images/Mongolia.png"></td>
<td><img src="images/Myanmar.png"></td>
<td><img src="images/Brazil.png"></td>
<td><img src="images/Italy.png"></td>
<td></td>
<td></td>
</tr>
<tr>
<td colspan="9" valign="top"><img src="images/studyjp_noflags.png"></td>
-->
</tr>
</table> 

## 概要

日本語倶楽部は、日本語を学ぶための任意同好会です。日本語、英語、中国語、ネパール語、インドネシア語、ベトナム語の 6 か国語に対応し、多文化の学習者を歓迎します。
## 対応言語
- 日本語
- 英語
- 中国語
- ネパール語
- インドネシア語
- ベトナム語

## 活動内容
第１回日本語勉強会 : ひらがな「あいうえお」のライティングと自己紹介スピーキング。グーチョキパーが初体験で興味深々盛り上がりました。<br>
<img src="images/img_1911.jpg" alt="jyugyou1" width="200px" height="150px">
<img src="images/img_1916.jpg" alt="jyugyou2" width="200px" height="150px">
<img src="images/img_1917.jpg" alt="jyugyou3" width="200px" height="150px">
<img src="images/img_1913.jpg" alt="jyugyou4" width="200px" height="150px">
<img src="images/img_1922.jpg" alt="jyugyou5" width="200px" height="150px">
<img src="images/img_1923.jpg" alt="jyugyou6" width="200px" height="150px">

第２回日本語勉強会：ひらがな書き取り小テストと自己紹介。名前と出身地。グーチョキパーがまだきごちない。<br>
<img src="images/img_1927.jpg" alt="jyugyou6" width="200px" height="150px">
<img src="images/img_1937.jpg" alt="jyugyou6" width="200px" height="150px">
<img src="images/img_1938.jpg" alt="jyugyou6" width="200px" height="150px">
<img src="images/img_1939.jpg" alt="jyugyou6" width="200px" height="150px">
<img src="images/img_1945.jpg" alt="jyugyou6" width="200px" height="150px">
<img src="images/img_1948.jpg" alt="jyugyou6" width="200px" height="150px">
<img src="images/img_1960.jpg" alt="jyugyou6" width="200px" height="150px">
<img src="images/img_1964.jpg" alt="jyugyou6" width="200px" height="150px"><br>
第３回日本語勉強会：ひらがな書き取り（濁音がぎぐげご）。自己紹介，私の趣味は〇〇です。<br>
最初はグー。ジャンケンポン。マスターしました。<br>
<img src="images/img_1983.jpg" alt="jyugyou6" width="200px" height="150px">
<img src="images/img_1984.jpg" alt="jyugyou6" width="200px" height="150px">
<img src="images/img_1985.jpg" alt="jyugyou6" width="200px" height="150px">
<img src="images/img_1987.jpg" alt="jyugyou6" width="200px" height="150px">
<img src="images/img_1993.jpg" alt="jyugyou6" width="200px" height="150px">
<img src="images/img_1996.jpg" alt="jyugyou6" width="200px" height="150px">
<img src="images/img_1997.jpg" alt="jyugyou6" width="200px" height="150px">
<img src="images/img_1999.jpg" alt="jyugyou6" width="200px" height="150px">
<img src="images/img_2001.jpg" alt="jyugyou6" width="200px" height="150px">
<img src="images/img_2007.jpg" alt="jyugyou6" width="200px" height="150px">
<img src="images/img_2009.jpg" alt="jyugyou6" width="200px" height="150px">
<img src="images/img_2018.jpg" alt="jyugyou6" width="200px" height="150px">
<img src="images/img_2019.jpg" alt="jyugyou6" width="200px" height="150px">
<img src="images/img_2025.jpg" alt="jyugyou6" width="200px" height="150px">
<img src="images/img_2029.jpg" alt="jyugyou6" width="200px" height="150px">
<img src="images/img_2038.jpg" alt="jyugyou6" width="200px" height="150px"><br>
第４回日本語勉強会：ひらがな書き取り（濁音，拗音），自己紹介および他の人を紹介。<br>
代表発表グループを“あみだくじ”で決めました。<br>
<img src="images/img_2042.jpg" alt="jyugyou6" width="200px" height="150px">
<img src="images/img_2043.jpg" alt="jyugyou6" width="200px" height="150px">
<img src="images/img_2045.jpg" alt="jyugyou6" width="200px" height="150px">
<img src="images/img_2049.jpg" alt="jyugyou6" width="200px" height="150px">
<img src="images/img_2058.jpg" alt="jyugyou6" width="200px" height="150px">
<img src="images/img_2062.jpg" alt="jyugyou6" width="200px" height="150px">
<img src="images/img_2064.jpg" alt="jyugyou6" width="200px" height="150px">
<img src="images/img_2068.jpg" alt="jyugyou6" width="200px" height="150px">
<img src="images/img_2071.jpg" alt="jyugyou6" width="200px" height="150px">
<img src="images/img_2073.jpg" alt="jyugyou6" width="200px" height="150px"><br>
第５回日本語勉強会：カタカナ書き取り（直音）。企業面接へ向けての自己紹介。<br>
「京都情報大学院大学（きょうとじょうほうだいがくいんだいがく）１年生です。」大学名長いなぁ。<br>
<img src="images/img_2074.jpg" alt="jyugyou6" width="200px" height="150px">
<img src="images/img_2077.jpg" alt="jyugyou6" width="200px" height="150px">
<img src="images/img_2081.jpg" alt="jyugyou6" width="200px" height="150px">
<img src="images/img_2083.jpg" alt="jyugyou6" width="200px" height="150px">
<img src="images/img_2086.jpg" alt="jyugyou6" width="200px" height="150px">
<img src="images/img_2087.jpg" alt="jyugyou6" width="200px" height="150px">
<img src="images/img_2088.jpg" alt="jyugyou6" width="200px" height="150px">
<img src="images/img_2090.jpg" alt="jyugyou6" width="200px" height="150px">
<img src="images/img_2098.jpg" alt="jyugyou6" width="200px" height="150px">
<img src="images/img_2099.jpg" alt="jyugyou6" width="200px" height="150px">
<img src="images/img_2100.jpg" alt="jyugyou6" width="200px" height="150px"><br>
第６回日本語勉強会：カタカナ書き取り（濁音・拗音）ガギグケゴ・キャキュキョ。企業面接へ向けての自己紹介
現在（げんざい）〇〇〇〇を勉強しています。「げんざい」の「ざ」の発音が難しい。
本日はけん玉初体験。意外とみんな苦戦。ひざを使ってクッションをつくるとうまくいきますよ。<br>
<img src="images/img_2102.jpg" alt="jyugyou6" width="200px" height="150px">
<img src="images/img_2107.jpg" alt="jyugyou6" width="200px" height="150px">
<img src="images/img_2108.jpg" alt="jyugyou6" width="200px" height="150px">
<img src="images/img_2109.jpg" alt="jyugyou6" width="200px" height="150px">
<img src="images/img_2111.jpg" alt="jyugyou6" width="200px" height="150px">
<img src="images/img_2115.jpg" alt="jyugyou6" width="200px" height="150px">
<img src="images/img_2117.jpg" alt="jyugyou6" width="200px" height="150px">
<img src="images/img_2118.jpg" alt="jyugyou6" width="200px" height="150px">
<img src="images/img_2122.jpg" alt="jyugyou6" width="200px" height="150px">
<img src="images/img_2124.jpg" alt="jyugyou6" width="200px" height="150px">
<img src="images/img_2127.jpg" alt="jyugyou6" width="200px" height="150px">
<img src="images/img_2128.jpg" alt="jyugyou6" width="200px" height="150px">
<img src="images/img_2130.jpg" alt="jyugyou6" width="200px" height="150px">
<img src="images/img_2131.jpg" alt="jyugyou6" width="200px" height="150px"><br>
第７回日本語勉強会：カレンダーの知識。日曜日，月曜日・・・「曜」の漢字は難しい。
曜日カルタゲーム「8月9日（はちがつここのか）は何曜日？」『金曜日』のカードが正解。<br>
<img src="images/img_2132.jpg" alt="jyugyou6" width="200px" height="150px">
<img src="images/img_2133.jpg" alt="jyugyou6" width="200px" height="150px">
<img src="images/img_2134.jpg" alt="jyugyou6" width="200px" height="150px">
<img src="images/img_2135.jpg" alt="jyugyou6" width="200px" height="150px">
<img src="images/img_2136.jpg" alt="jyugyou6" width="200px" height="150px">
<img src="images/img_2139.jpg" alt="jyugyou6" width="200px" height="150px">
<img src="images/img_2141.jpg" alt="jyugyou6" width="200px" height="150px">
<img src="images/img_2142.jpg" alt="jyugyou6" width="200px" height="150px">
<img src="images/img_2144.jpg" alt="jyugyou6" width="200px" height="150px">
<img src="images/img_2145.jpg" alt="jyugyou6" width="200px" height="150px">
<img src="images/img_2146.jpg" alt="jyugyou6" width="200px" height="150px"><br>
第８回日本語勉強会：カレンダーの知識。第二日曜日は何日？。8月4日（よっか）は何曜日？<br>
自己紹介インタビュー。「出身」（しゅっしん）の発音が母国にないので苦戦することが新しい発見。<br>
「しゅっしん」が「すっしん」になる。母国語には促音「っ」がない？<br>
学生からの要望で，「が」と「わ」の使い分けを勉強しました。教える方も難しい。<br>

<img src="images/img_2147.jpg" alt="jyugyou6" width="200px" height="150px">
<img src="images/img_2151.jpg" alt="jyugyou6" width="200px" height="150px">
<img src="images/img_2153.jpg" alt="jyugyou6" width="200px" height="150px">
<img src="images/img_2167.jpg" alt="jyugyou6" width="200px" height="150px">
<img src="images/img_2169.jpg" alt="jyugyou6" width="200px" height="150px">
<img src="images/img_2162.jpg" alt="jyugyou6" width="200px" height="150px">
<img src="images/img_2164.jpg" alt="jyugyou6" width="200px" height="150px">
<img src="images/img_2166.jpg" alt="jyugyou6" width="200px" height="150px"><br>
<br>
第９回以降「日本語勉強会」の画像はインスタグラムに掲載しています。よろしくお願いします。<br>
https://www.instagram.com/nihongoclubkyoto/<br>
ホームページ左上のinstagramをクリックしてください。<br>
<br>

### 日本語勉強会
日本語倶楽部では、定期的に日本語の勉強会を開催しています。初級から上級まで、各レベルに合わせたクラスがあります。
### スタッフのつぶやき
京都では6月30日に神社で夏越の祓い。茅の和ぐぐりが行われます。<br>
半年の厄を払い、残り半年の無事を願う神事です。明日30日は所用のためフライングで29日にくぐってきました。<br>
建勲神社より。shuichi　<br>
<img src="images/chinowakuguri1.jpg" alt="jyugyou6" width="200px" height="300px">
<img src="images/chinowakuguri2.jpg" alt="jyugyou6" width="200px" height="300px">
<img src="images/chinowakuguri3.jpg" alt="jyugyou6" width="200px" height="300px"><br>
https://ja.kyoto.travel/event/single.php?event_id=6772<br>

7月の京都といえば「祇園祭」。7月1日からの吉符入に始まり，山鉾巡行などの行事が行われます。
年々この季節の気温が上昇していますので熱中症に気をつけながら様々な山鉾のスタイルを見ることをお勧めします。
https://ja.kyoto.travel/event/single.php?event_id=6603<br>

ネパールの学生を中心に好きな映画は？と聞いたら「タイタニック」などのハリウッド映画の回答が多かったですが，<br>
母国のおすすめ映画は？と聞くと「バーフバリ」がおすすめだと教えてくれました。日本語字幕のDVDもありました。<br>
ためしに「バーフバリ 伝説誕生」を視聴しました。インド映画。とにかく長い。138分。でも面白かったです。<br>
教えてくれた留学生のみなさん。ありがとう。<br>
<img src="images/baahubali.jpg" alt="jyugyou6" width="150px" height="150px"><br>

先日の『日本語倶楽部』勉強会では，グループ発表者を「あみだくじ」で決めました。
これで「じゃんけん」と「あみだくじ」，二つのゲームを覚えました。
「じゃんけん」の起源は祇園などのお茶屋さんの「虎拳」。「あみだくじ」は室町時代から始まって，最初は放射線状に
線を引いたので，阿弥陀如来の後光のようだったので，阿弥陀くじと言われるそうです。<br>
<img src="images/amidakuji.jpg" alt="jyugyou6" width="150px" height="150px"><br>
第５回日本語勉強会では，今宮神社に伝わる『玉の輿』について説明しました。ちょっと難しかった？<br>
江戸時代。八百屋の娘の玉さんは三代将軍・徳川家光に見初められ側室となり、五代将軍・綱吉を生みました。<br>
玉の輿（たまのこし）は、女性が社会的地位のある男性や資産家の男性と結婚することにより、自分も社会的地位を手に入れたり
裕福な立場になること。男性が金持ちの女性と結婚する場合は、俗に「逆玉（ぎゃくたま）」と呼ばれる・・・らしい。<br>

祇園祭　後祭山鉾巡行
7月24日は祇園祭　後祭山鉾巡行です。写真は役行者山。前祭のような派手さはないですが，それぞれの鉾町の心意気が
みられる味わい深い行事です。<br>
<img src="images/ennogyojyayama1.jpg" alt="jyugyou6" width="100px" height="120px"><br>
https://www.kyotodeasobo.com/gion/

8月です。夏休みに入りました。毎年毎年「観測史上過去最高気温」との発表。暑いですねぇ。<br>
このコラムを書いていると「暑い」と「熱い」の違いはどう説明する？なんてことも気になってしまいます。<br>
みなさん体調に気を付けて。夏休み中も『日本語勉強会』継続の声あり。みんなほんとに来るのかなぁ。<br>

8月お盆休み前の日本語勉強会。夏休みも日本語が勉強したいとの声を受けて勉強会を実施。
「出身」（しゅっしん）の「しゅ」「っ」促音が母国語にないので発話に苦戦。
日本人ももちろん母国語にない発話は難しいのと同じ。共感しました。がんばれ。

写真投稿が多くなってきたのでインスタグラムinstagram始めました。<br>
https://www.instagram.com/nihongoclubkyoto/
ホームページ左上のinstagramをクリックしてください。<br>


お盆休みにインド映画のＤＶＤを１本見ました。邦題『エンドロールのつづき』原題「last film show」<br>
第９５回アカデミー賞国際長編映画賞受賞。パン・ナリン監督の自伝的映画。ニューシネマパラダイスを思い出させる映画でした。<br>
<img src="images/lastfilmshow.jpg" alt="jyugyou1" width="200px" height="150px">

京都には「地蔵盆」という風習があります。詳しくは上記インスタグラムにアップしました。<br>
<img src="images/img_2170.jpg" alt="jyugyou6" width="200px" height="150px"><br>

### 活動風景

### 文化交流イベント
日本文化に触れる機会を提供するために、様々なイベントを開催予定です。


## メンバー紹介

<!-- 
![Shuichi](images/shuichi.jpg)
-->
### Shuichi
<img src="images/shuichi.jpg" alt="Shuichi" width="150px" height="200px">

<!-- 
![Suraj](images/suraj.jpg)
-->
### Suraj
<img src="images/suraj.jpg" alt="suraj" width="150px" height="200px">
<!--
<img src="images/xxxx.jpg" alt="xxxx" width="150px" height="200px">
<img src="images/xxxx.jpg" alt="xxxx" width="150px" height="200px">
-->
<br>
<div align="right">Horie (Web Adviser)</div>
<div align="right"><img src="images/horie.jpg" alt="Horie" width="90px" height="120px"></div>

## 参加方法

### ステップ1: 登録
参加をご希望の方は、直接声をかけてください。

<!-- [登録フォーム](path/to/registration_form) -->

### ステップ2: 初回ミーティング
登録後、初回ミーティングのスケジュールをお送りします。ミーティングでは、倶楽部の活動内容やルールについてご説明します。

### ステップ3: 活動開始
初回ミーティングが終わったら、すぐに倶楽部の活動に参加できます。イベントや勉強会のスケジュールは、専用のオンラインカレンダーで確認できます。

## お問い合わせ

ご質問やお問い合わせは、以下のメールアドレスまでご連絡ください。

[nihongoclub888@gmail.com](mailto:nihongoclub888@gmail.com)

<!--
## SNS

最新情報やイベント告知は、以下のSNSでもご覧いただけます。

- [Facebook](https://www.facebook.com/nihongoclub)
- [Twitter](https://www.twitter.com/nihongoclub)
- [Instagram](https://www.instagram.com/nihongoclub)
-->

---

© 2024 日本語倶楽部. All rights reserved.

<!-- Google tag (gtag.js) -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-RJW7DND1MW"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-RJW7DND1MW');
</script>